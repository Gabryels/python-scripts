#modules
import argparse
import requests
from bs4 import BeautifulSoup

def reqMethod():
    #request host especified on args.host in main function
    try:
        request = requests.get(args.host)
    except requests.exceptions.MissingSchema:
        print("Please insert a correct url: ex: http://example.com")
        exit()

    objSoup = BeautifulSoup(request.content)
    #searching for occurrence attributs in html font code
    for link in objSoup.findAll(args.tag):
        if args.attributs in link.attrs:
            print(link.attrs[args.attributs])
#man function, responsible for arguments command line
def main():
    global args

    parser = argparse.ArgumentParser(description='htmlanalizer is a simple script write in python3 that looks for occurrences of files in the html source code of the page given a regular expression, example: src that would be the attribute of the tag, so htmlanalizer can easily go through the source code looking for all the files in the tags whose attribute is src.')
    parser.add_argument('-a', '--attributs', help="Especified attributs on tags", type=str)        #attributs for tags in html font code
    parser.add_argument('-hs', '--host', help="Especified host for searching")                     #host for request
    parser.add_argument('-t', '--tag', help="Especified tag for searching attributs on", type=str)
    args = parser.parse_args()               #args object

    function = reqMethod()
    print(function)

    return 0

if __name__ == '__main__':
    main()

