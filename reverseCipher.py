#you message
msg = str(input("msg: ")).strip()

#encode logic
translated = ''
i = len(msg) - 1
while i >= 0:
    translated = translated + msg[i]
    i -= 1

print(translated)

#decode logic
decode = ''
d = len(translated) - 1
while d >= 0:
    decode = decode + translated[d]
    d -= 1

print(decode)
