import argparse
import urllib
from urllib.request import urlopen

def extractHTTPHeader():
    try:
        url = urlopen(args.host)
        response_headers = url.info()
        return response_headers[args.head]
    except IndexError:
        exit()
    except AttributeError:
        return response_headers

def main():
    global args

    parser = argparse.ArgumentParser(description="httpextract is a very simple script, created in order to separate http headers to perhaps redirect the output to some other command, example of a cookie encoded in base64 in the response header: python3 httpextract.py -hd set-cookie --host http: //example.com | base64 -d, this would decode the set-cookie value and return to the terminal.")
    parser.add_argument('-hs', '--host')
    parser.add_argument('-hd', '--head')
    args = parser.parse_args()

    print(extractHTTPHeader())

if __name__ == "__main__":
    main()

