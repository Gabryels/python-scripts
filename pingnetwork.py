import subprocess
import sys

def pingNtwk(networkClass):
    hostsUp = []
    PING = "ping -c1"
    i = 0
    for host in range(1, 256+1):
        try:
            result = subprocess.check_output(PING + " {}.{}".format(networkClass, host), shell=True)
            hostsUp.append(networkClass + ".{}".format(host))
            print("UP HOST: {}".format(hostsUp[i]))
            i += 1
        except subprocess.CalledProcessError:
            continue
        except KeyboardInterrupt:
            break
    print("\033[34mTotal Hosts: {}\033[m".format(len(hostsUp)))

def main():
    pingNtwk(sys.argv[1])
    
if __name__ == "__main__":
    main()